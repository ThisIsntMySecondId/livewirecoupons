<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('url')->nullable();
            $table->string('logo')->nullable();
            $table->string('country')->nullable();
            $table->json('available_banks')->nullable();
            $table->json('available_brands')->nullable();
            $table->json('available_categories')->nullable();
            $table->json('available_tags')->nullable();
            $table->json('similar_stores')->nullable();
            $table->json('similar_coupons')->nullable();
            $table->json('cashback_terms')->nullable();
            $table->json('reward_rates')->nullable();
            $table->float('popularity', 4, 2)->nullable();
            $table->float('highest_cashback', 4, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
