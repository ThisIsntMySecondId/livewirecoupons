<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->enum('coupon_type', ['cashback', 'offer', 'deals'])->default('offer');
            $table->enum('reward_type', ['flat', 'upto', 'percent', 'na'])->default('na');
            $table->float('reward_rate')->nullable();
            $table->string('code')->nullable();
            $table->boolean('verified')->default(false);
            $table->float('success_rate')->nullable();
            $table->unsignedBigInteger('store_id');
            $table->json('applicable_brands')->nullable();
            $table->json('applicable_categories')->nullable();
            $table->json('applicable_tags')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
