<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bank;
use Faker\Generator as Faker;

$factory->define(Bank::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'logo' => "https://sisterhoodofstyle.com/wp-content/uploads/2018/02/no-image-1.jpg",
    ];
});
