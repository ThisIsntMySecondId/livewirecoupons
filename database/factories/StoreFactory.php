<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bank;
use App\Brand;
use App\Category;
use App\Store;
use App\Tag;
use Faker\Generator as Faker;

$factory->define(Store::class, function (Faker $faker) {
    $available_banks = Bank::inRandomOrder()->take($faker->numberBetween(1,7))->pluck('id');
    $available_brands = Brand::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    $available_categories = Category::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    $available_tags = Tag::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    return [
        'name' => $faker->company,
        "description" => $faker->realText(200),
        "url" => $faker->domainName,
        "logo" => "https://picsum.photos/200/300",
        "country" => $faker->country,
        "available_banks" => json_encode($available_banks),
        "available_brands" => json_encode($available_brands),
        "available_categories" => json_encode($available_categories),
        "available_tags" => json_encode($available_tags),
        "similar_stores" => "",
        "similar_coupons" => "",
        "cashback_terms" => "",
        "reward_rates" => "",
        'popularity' => $faker->numberBetween($min = 1000, $max = 9000) / 100,
        'highest_cashback' => $faker->numberBetween($min = 100, $max = 9000) / 100,
    ];
});
