<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Brand;
use App\Category;
use App\Coupon;
use App\Store;
use App\Tag;
use Faker\Generator as Faker;

$factory->define(Coupon::class, function (Faker $faker) {
    $store_id = Store::inRandomOrder()->take(1)->get()[0]->id;
    $available_brands = Brand::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    $available_categories = Category::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    $available_tags = Tag::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    return [
        'title' => $faker->realText(100, 1),
        'description' => $faker->realText(250, 3),
        'coupon_type' => $faker->randomElement(['cashback', 'offer', 'deals']),
        'reward_type' => $faker->randomElement(['flat', 'upto', 'percent', 'na']),
        'reward_rate' => $faker->numberBetween(0,100) / 10,
        'code' => $faker->regexify('[A-Z0-9]{6,8}'),
        'verified' => $faker->boolean, 
        'success_rate' => $faker->numberBetween(0,100) / 10,
        'store_id' => $store_id,
        'applicable_brands' => json_encode($available_brands),
        'applicable_categories' => json_encode($available_categories),
        'applicable_tags' => json_encode($available_tags),
    ];
});
