<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Brand;
use App\Category;
use App\Tag;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Brand::class, function (Faker $faker) {
    $availableCategories = Category::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    $availableTags = Tag::inRandomOrder()->take($faker->numberBetween(3,10))->pluck('id');
    return [
        'name' => $faker->company,
        "available_categories" => json_encode($availableCategories),
        "available_tags" => json_encode($availableTags),
    ];
});
