<?php

use App\Brand;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(BankSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(StoreSeeder::class);
        $this->call(CouponSeeder::class);
    }
}
