<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $guarded = [];

    protected $casts = [
        'applicable_brands' => 'array',
        'applicable_categories' => 'array',
        'applicable_tags' => 'array',
    ];

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
