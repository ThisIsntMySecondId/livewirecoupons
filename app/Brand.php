<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $guarded = [];

    protected $casts = [
        'available_categories' => 'array',
        'available_tags' => 'array',
    ];
}
