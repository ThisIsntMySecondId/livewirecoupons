<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CouponCard extends Component
{

    public $coupon;

    public function mount($coupon) {
        $this->coupon = $coupon;
    }

    public function toggleBookmarkStatus($couponId) {
        if(in_array($couponId, Auth::user(1)->bookmarked_coupons)) {
            $updatedCouponMarks = array_filter(Auth::user(1)->bookmarked_coupons, function ($item) use ($couponId) {
                return $item !== $couponId;
            });
        } else {
            $updatedCouponMarks = Auth::user(1)->bookmarked_coupons;
            $updatedCouponMarks[] = $couponId;
        }
        Auth::user(1)->bookmarked_coupons = $updatedCouponMarks;
        Auth::user(1)->save();
    }

    public function render()
    {
        return view('livewire.coupon-card');
    }
}
