<?php

namespace App\Http\Livewire;

use App\Brand;
use App\Category;
use App\Coupon;
use App\Tag;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CouponList extends Component
{
    public $coupons;
    public $filteredCategories = [];
    public $filteredBrands = [];
    public $filteredTags = [];

    protected $listeners = [
        'filteredCategoriesUpdated' => 'handleCategoryFilterChange',
        'filteredBrandsUpdated' => 'handleBrandFilterChange',
        'filteredTagsUpdated' => 'handleTagFilterChange',
    ];

    public function mount() {
        $this->coupons = Coupon::take(30)->get();
    }

    public function handleCategoryFilterChange($filteredCategoriesPayload) {
        if(count($filteredCategoriesPayload) > 0) {
            $this->filteredCategories = Category::find($filteredCategoriesPayload);
            $str = 'JSON_OVERLAPS(applicable_categories->>"$", "' . json_encode(array_map('intval', $filteredCategoriesPayload)) . '")';
            $this->coupons = Coupon::whereRaw($str)->get();
        } else {
            $this->coupons = Coupon::take(30)->get();
            $this->filteredCategories = [];
        }
    }

    public function handleBrandFilterChange($filteredBrandsPayload) {
        if(count($filteredBrandsPayload) > 0) {
            $this->filteredBrands = Brand::find($filteredBrandsPayload);
            $str = 'JSON_OVERLAPS(applicable_brands->>"$", "' . json_encode(array_map('intval', $filteredBrandsPayload)) . '")';
            $this->coupons = Coupon::whereRaw($str)->get();
        } else {
            $this->coupons = Coupon::take(30)->get();
            $this->filteredBrands = [];
        }
    }

    public function handleTagFilterChange($filteredTagsPayload) {
        if(count($filteredTagsPayload) > 0) {
            $this->filteredTags = Tag::find($filteredTagsPayload);
            $str = 'JSON_OVERLAPS(applicable_Tags->>"$", "' . json_encode(array_map('intval', $filteredTagsPayload)) . '")';
            $this->coupons = Coupon::whereRaw($str)->get();
        } else {
            $this->coupons = Coupon::take(30)->get();
            $this->filteredTags = [];
        }
    }

    // public function toggleBookmarkStatus($couponId) {
    //     if(in_array($couponId, Auth::user(1)->bookmarked_coupons)) {
    //         $updatedCouponMarks = array_filter(Auth::user(1)->bookmarked_coupons, function ($item) use ($couponId) {
    //             return $item !== $couponId;
    //         });
    //     } else {
    //         $updatedCouponMarks = Auth::user(1)->bookmarked_coupons;
    //         $updatedCouponMarks[] = $couponId;
    //     }
    //     Auth::user(1)->bookmarked_coupons = $updatedCouponMarks;
    //     Auth::user(1)->save();
    // }

    public function render()
    {
        return view('livewire.coupon-list');
    }
}
