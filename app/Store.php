<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $guarded = [];

    protected $casts = [
        'available_banks' => 'array',
        'available_brands' => 'array',
        'available_categories' => 'array',
        'available_tags' => 'array',
        'similar_stores' => 'array',
        'similar_coupons' => 'array',
        'cashback_terms' => 'array',
        'reward_rates' => 'array'
    ];

    public function coupons()
    {
        return $this->hasMany('App\Coupon');
    }
}
