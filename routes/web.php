<?php

use App\Brand;
use App\Category;
use App\Store;
use App\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/coupons', function () {
    $categories = Category::all();
    $brands = Brand::all();
    $tags = Tag::all();
    return view('coupons', compact('categories', 'brands', 'tags'));
});

Route::get('/stores', function() {
    $stores = Store::all();
    // $stores = Store::take(3)->get();
    $categories = Category::all();
    return view('store', compact('stores', 'categories'));
});

Route::view('/counter', 'welcome');
Route::get('/home', 'HomeController@index')->name('home');
