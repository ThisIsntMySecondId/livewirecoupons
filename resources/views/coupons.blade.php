@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Welcome</h1>
    <div class="row">
        <div class="col-3">
            <x-category-filter :categories="$categories" />
            <x-brand-filter :brands="$brands" />
            <x-tag-filter :tags="$tags" />
        </div>
        <div class="col-9">
            <livewire:coupon-list/>
        </div>
    </div>
</div>
@endsection
