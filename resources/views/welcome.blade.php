@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Welcome</h1>
    <div class="row">
        <div class="col">
            <livewire:counter />
        </div>
        <div class="col">
            <x-alpine-counter />
        </div>
    </div>
</div>
@endsection
