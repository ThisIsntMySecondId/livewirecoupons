<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <livewire:styles />

    @stack('styles')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <p class="display-4">Livewire Demo</p>
                <a class="lead {{ Request::is('counter') ? 'font-weight-bold' : '' }}" href="/counter">counter</a>
                <a class="lead {{ Request::is('coupons') ? 'font-weight-bold' : '' }}" href="/coupons">coupons</a>
                <a class="lead {{ Request::is('stores') ? 'font-weight-bold' : '' }}" href="/stores">stores</a>
                @guest
                    <a class="lead {{ Request::is('login') ? 'font-weight-bold' : '' }}" href="/login">Login</a>
                    <a class="lead {{ Request::is('register') ? 'font-weight-bold' : '' }}" href="/register">Register</a>
                @else
                    <a class="lead" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    <a class="lead" href="#">{{Auth::user()->email}}</a>
                @endguest

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <livewire:scripts />
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    @stack('scripts')
</body>
</html>
