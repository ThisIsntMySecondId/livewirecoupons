@extends('layouts.app')

@section('content')
<div x-data="setupStore()" class="container">
    <div class="row">
        <div class="col-3 border border-gray rounded m-3 bg-white">
            <div class="row pt-4">
                <div class="col">
                    <p class="h4"><strong>Filter Store By Categories</strong></p>
                </div>
            </div>
            <div class="row pt-4 border-top">
                <div class="col">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Search Categories" x-model="searchCategory">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary" type="button" 
                                  @click="searchCategory='';  currentSelectCategory = ''; filterApplied.category = undefined;">reset</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-4 border-top">
                <div class="col">
                    <ul class="list-group w-100">
                        @foreach ($categories as $category)
                        <li 
                            x-show="showCategory(`{{$category->name}}`)"
                            style="cursor: pointer;" 
                            class="list-group-item" 
                            :class="{'active': filterApplied.category && filterApplied.category.payload == '{{$category->id}}' }" 
                            value="{{$category->id}}" @click="filterCoupons('category', $event.target.value); currentSelectCategory = $event.target.innerText; "> {{$category->name}} </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-7 border border-gray rounded m-3 bg-white">
            <div class="row p-2 pt-4">
                <div class="col-9">
                    <div x-show="currentSelectCategory.length <= 0" class="col-6 h2">Earn Exiting Cashback</div>
                    <div x-show="currentSelectCategory.length > 0" class="col-6 h2">Stores under <span x-text="currentSelectCategory"></span> category</div>
                </div>
                <div class="col-3">
                    <select class="custom-select custom-select-sm mb-3" @change="sortStores($event.target.value, $event.target.value === 'name' ? 1 : -1 )">
                        <option value="name">Alphabetical</option>
                        <option value="popularity">Popularity</option>
                        <option value="highest_cashback">Cashback</option>
                    </select>
                </div>
            </div>
            <div class="row p-2 pt-4 border-top">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search Stores" x-model="storeSearchQuery" @input="filterCoupons('search', $event.target.value)">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="button">Search</button>
                    </div>
                </div>
                <p x-text="storeSearchQuery"></p>
            </div>
            <div class="row p-2 pt-4 border-top">
                <button class="mx-1 btn" 
                        :class="{ 'btn-primary': filterApplied.startingChar && filterApplied.startingChar.payload == 'all' }"
                        value="all" @click="filterCoupons('startingChar', $event.target.value)">All</button>
                <template x-for="letter in alphabetsArray">
                    <button class="mx-1 btn" 
                            :class="{ 'btn-primary': filterApplied.startingChar && filterApplied.startingChar.payload == letter }" 
                            :value="letter" @click="filterCoupons('startingChar', $event.target.value)" x-text="letter"></button>
                </template>
                <button class="mx-1 btn"
                        :class="{ 'btn-primary': filterApplied.startingChar && filterApplied.startingChar.payload == '0-9' }" 
                        value="0-9" @click="filterCoupons('startingChar', $event.target.value)">0-9</button>
            </div>
            <div class="row p-2 pt-4 border-top">
                <ul class="list-group w-100">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <div>Logo</div>
                        <div>Name</div>
                        <div>Popularity</div>
                        <div>Highest Cashback</div>
                        <div>Store Links</div>
                    </li>
                    <template x-for="store in stores">
                        <li x-show="showStore(store)" class="list-group-item">
                            <div class="position-relative d-flex justify-content-between align-items-center">
                                <div><img width="70px" height="70px" :src="store.logo" class="rounded-circle" :alt="store.name"></div>
                                <div style="width: 100px;">
                                    <span x-text="store.name"></span> 
                                    <div class="d-flex">
                                        <div style="cursor: pointer;"><x-icons.info /></div>
                                        <div style="cursor: pointer;"><x-icons.bookmark /></div>
                                    </div>
                                </div>
                                <div style="width: 100px;" x-text="store.popularity"></div>
                                <div style="width: 100px;" x-text="store.highest_cashback"></div>
                                <div><a :href="store.url" class="btn btn-primary">Go To Store</a></div>
                            </div>
                        </li>
                    </template>
                </ul>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        window.setupStore = function () {
            return {
                stores: {!!$stores!!},
                storeSearchQuery: '',
                alphabetsArray: Array.from(String.fromCharCode(...Array(26).fill(null).map((_, i) => i+65))),
                filterApplied: {},
                isLoding: false,
                currentSelectCategory: '',
                searchCategory: '',
                filterCoupons(filterType, filterPayload) {
                    this.filterApplied[filterType] = {
                        active: true,
                        payload: filterPayload,
                    }
                },
                showCategory(categroyName) {
                    return categroyName.toLowerCase().includes(this.searchCategory.toLowerCase());
                },
                showStore(storeDetails) {
                    let storeShown = true;
                    if(this.filterApplied.search && this.filterApplied.search.active) {
                        storeShown = storeShown && storeDetails.name.toLowerCase().includes(this.filterApplied.search.payload.toLowerCase());
                    }
                    if(this.filterApplied.startingChar && this.filterApplied.startingChar.active) {
                        switch(this.filterApplied.startingChar.payload) {
                            case 'all':
                                storeShown = storeShown && true;
                                break;
                            case '0-9':
                                storeShown = storeShown && '0123456789'.includes(storeDetails.name.toLowerCase().charAt(0));
                                break;
                            default: 
                                storeShown = storeShown && storeDetails.name.toLowerCase().startsWith(this.filterApplied.startingChar.payload.toLowerCase());
                        }
                    }
                    if(this.filterApplied.category && this.filterApplied.category.active) {
                        storeShown = storeShown && JSON.parse(storeDetails.available_categories).includes(this.filterApplied.category.payload)
                    }
                    return storeShown;
                },
                sortStores(field, order) {
                    this.stores = this.stores.sort((s1, s2) => {
                        if(s1[field] > s2[field]) return 1*order
                        if(s1[field] < s2[field]) return -1*order
                        return 0
                    });
                }
            };
        };
    </script>
@endpush
@endsection