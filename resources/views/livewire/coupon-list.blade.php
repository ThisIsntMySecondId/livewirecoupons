<div>
    <div>
        <p class="h3"> Filters </p>
        @auth
        <p> User's Bookmarked Coupons {{implode(", ", Auth::user()->bookmarked_coupons)}} </p>
        @endauth
        <div>
            <p class="strong border-bottom border-secondary mb-1">Filtered Categories </p>
            @foreach ($filteredCategories as $filteredCategory)   
            <button type="button" class="btn btn-sm btn-primary m-1" 
                    onclick="window.dispatchEvent(new CustomEvent('remove-category-from-category-filter', {detail: {payload: '{{$filteredCategory->id}}'}}))">
                {{$filteredCategory->name}} 
                <x-icons.close />
            </button>
            @endforeach
        </div>
        <div>
            <p class="strong border-bottom border-secondary mb-1">Filtered Brands </p>
            @foreach ($filteredBrands as $filteredBrand)   
            <button type="button" class="btn btn-sm btn-success m-1" 
                    onclick="window.dispatchEvent(new CustomEvent('remove-brand-from-brand-filter', {detail: {payload: '{{$filteredBrand->id}}'}}))">
                {{$filteredBrand->name}} 
                <x-icons.close />
            </button>
            @endforeach
        </div>
        <div>
            <p class="strong border-bottom border-secondary mb-1">Filtered Tags </p>
            @foreach ($filteredTags as $filteredTag)   
            <button type="button" class="btn btn-sm btn-danger m-1" 
                    onclick="window.dispatchEvent(new CustomEvent('remove-tag-from-tag-filter', {detail: {payload: '{{$filteredTag->id}}'}}))">
                {{$filteredTag->name}} 
                <x-icons.close />
            </button>
            @endforeach
        </div>
    </div>
    <div class="spinner-border" wire:loading></div>
    <div class="mt-4" wire:loading.remove>
        <p class="h3"> Listing </p>
        <div style="display: grid; grid-template-columns: repeat(3, 1fr); grid-template-rows: auto;">
            @foreach ($coupons as $coupon)
            <livewire:coupon-card :coupon="$coupon" :key="'coupon_' . $coupon->id" />
            @endforeach
        </div>
        {{-- <div wire:loading>
            <img width="100%" src="https://miro.medium.com/max/882/1*9EBHIOzhE1XfMYoKz1JcsQ.gif" alt="Loading Categories..." />
        </div> --}}
    </div>
</div>