<div class="p-2 mb-3 bg-info text-white d-flex flex-column justify-content-between rounded" style="width: 250px; min-height: 200px;">
    <p class="h4">{{$coupon->id}} => {{$coupon->title}}</p>
    <div class="row">
        <div class="col">50% off</div>
        <div class="col font-weight-bold">{{$coupon->code}}</div></div>
    <div class="row">
        <div class="col">Store Name:</div>
        <div class="col">{{$coupon->store->name}}</div>
    </div>
    <div class="row">
        <div class="col d-flex w-100 d-flex justify-content-between">
            <div>
                @if ($coupon->verified)
                    <x-icons.check-circle /> 
                @else
                    <x-icons.slash /> 
                @endif
                <span>verified</span>
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <x-coupon-info-popup :categoryIds="$coupon->applicable_categories" :brandIds="$coupon->applicable_brands" :storeIds="$coupon->store_id" :tagIds="$coupon->applicable_tags" />
                @auth
                <div style="cursor: pointer;" wire:click="toggleBookmarkStatus({{$coupon->id}})">
                    @if (in_array($coupon->id, Auth::user()->bookmarked_coupons))
                        <x-icons.bookmark marked />
                    @else
                        <x-icons.bookmark />
                    @endif
                </div>
                @endauth
            </div>
        </div>
    </div>
</div>