<div 
    x-data="{ categories: {{$categories}}, searchQuery: '', selectedCategories: [] }" 
    x-init="$watch('selectedCategories', value => window.livewire.emit('filteredCategoriesUpdated', selectedCategories))"
    @remove-category-from-category-filter.window="selectedCategories = selectedCategories.filter(item => item !== $event.detail.payload)"
>
    <p class="lead border-bottom border-secondary"> Category Filter </p>

    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Category" x-model="searchQuery">
        <template x-if="searchQuery">
            <div class="input-group-append">
                <button class="btn btn-outline-info" type="button" @click="searchQuery = ''">X</button>
            </div>
        </template>
        <div class="input-group-append">
            <button class="btn btn-outline-primary" type="button">Go</button>
        </div>
        <div class="input-group-append">
            <button class="btn btn-outline-danger" type="button" @click="selectedCategories = []">Clear</button>
        </div>
    </div>

    <div class="mt-2 pl-2 border border-secondary w-100" style="height: 300px; overflow-y: scroll;">
        <template x-for="category in categories" :key="category">
            <div class="form-check" x-show="searchQuery ? category.name.toLowerCase().includes(searchQuery.toLowerCase()) : true">
                <input 
                    type="checkbox" 
                    class="form-check-input" 
                    :id="`cat_${category.id}`" 
                    :value="`${category.id}`" 
                    x-model="selectedCategories"
                >
                <label style="cursor: pointer;" class="form-check-label" :for="`cat_${category.id}`" x-text="category.name"></label>
            </div>
        </template>
    </div>
    <p> selected = <span x-text="selectedCategories" ></span> </p>
</div>
