<div style="text-align: center" x-data="{ count: 0 }">
    <button class="btn btn-primary" @click="count++">+</button>
    <h1>From Alpine <span x-text='count'></span></h1>
    <button class="btn btn-primary" @click="count--">-</button>
</div>