<div x-data="{isShown: false}" style="cursor: pointer;" class="inline-block position-relative">
    <div @click="isShown = true"><x-icons.info /></div>
    <div 
        style="min-height: 100px; min-width: 250px; bottom: 100%; transform: translateX(-50%); z-index: 11; display: none;" 
        class="m-2 p-2 pb-4 bg-white text-dark position-absolute rounded border border-primary" x-show="isShown" @click.away="isShown = false"
    >
        <div class="row">
            <div class="col">Category Ids</div>
            <div class="col">{{$categoryIds}}</div>
        </div>
        <div class="row">
            <div class="col">Brand Ids</div>
            <div class="col">{{$brandIds}}</div>
        </div>
        <div class="row">
            <div class="col">Store Ids</div>
            <div class="col">{{$storeIds}}</div>
        </div>
        <div class="row">
            <div class="col">Tags Ids</div>
            <div class="col">{{$tagIds}}</div>
        </div>
        <div style="position: absolute;
                    bottom: -10px;
                    left: 50%;
                    transform: rotate(180deg);
                    z-index: -11;" 
            class="text-white">
            <x-icons.triangle fill="white"/>
        </div>
    </div>
</div>