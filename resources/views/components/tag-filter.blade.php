<div 
    x-data="{ tags: {{$tags}}, searchQuery: '', selectedTags: [] }" 
    x-init="$watch('selectedTags', value => window.livewire.emit('filteredTagsUpdated', selectedTags))"
    @remove-tag-from-tag-filter.window="selectedTags = selectedTags.filter(item => item !== $event.detail.payload)"
>
    <p class="lead border-bottom border-secondary"> Tag Filter </p>

    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Tag" x-model="searchQuery">
        <template x-if="searchQuery">
            <div class="input-group-append">
                <button class="btn btn-outline-info" type="button" @click="searchQuery = ''">X</button>
            </div>
        </template>
        <div class="input-group-append">
            <button class="btn btn-outline-primary" type="button">Go</button>
        </div>
        <div class="input-group-append">
            <button class="btn btn-outline-danger" type="button" @click="selectedTags = []">Clear</button>
        </div>
    </div>

    <div class="mt-2 pl-2 border border-secondary w-100" style="height: 300px; overflow-y: scroll;">
        <template x-for="tag in tags" :key="tag">
            <div class="form-check" x-show="searchQuery ? tag.name.toLowerCase().includes(searchQuery.toLowerCase()) : true">
                <input 
                    type="checkbox" 
                    class="form-check-input" 
                    :id="`tag_${tag.id}`" 
                    :value="`${tag.id}`" 
                    x-model="selectedTags"
                >
                <label style="cursor: pointer;" class="form-check-label" :for="`tag_${tag.id}`" x-text="tag.name"></label>
            </div>
        </template>
    </div>
    <p> selected = <span x-text="selectedTags" ></span> </p>
</div>
