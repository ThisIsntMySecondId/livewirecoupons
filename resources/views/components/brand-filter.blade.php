<div 
    x-data="{ brands: {{$brands}}, searchQuery: '', selectedBrands: [] }" 
    x-init="$watch('selectedBrands', value => window.livewire.emit('filteredBrandsUpdated', selectedBrands))"
    @remove-brand-from-brand-filter.window="selectedBrands = selectedBrands.filter(item => item !== $event.detail.payload)"
>
    <p class="lead border-bottom border-secondary"> Brand Filter </p>

    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Brand" x-model="searchQuery">
        <template x-if="searchQuery">
            <div class="input-group-append">
                <button class="btn btn-outline-info" type="button" @click="searchQuery = ''">X</button>
            </div>
        </template>
        <div class="input-group-append">
            <button class="btn btn-outline-primary" type="button">Go</button>
        </div>
        <div class="input-group-append">
            <button class="btn btn-outline-danger" type="button" @click="selectedBrands = []">Clear</button>
        </div>
    </div>

    <div class="mt-2 pl-2 border border-secondary w-100" style="height: 300px; overflow-y: scroll;">
        <template x-for="brand in brands" :key="brand">
            <div class="form-check" x-show="searchQuery ? brand.name.toLowerCase().includes(searchQuery.toLowerCase()) : true">
                <input 
                    type="checkbox" 
                    class="form-check-input" 
                    :id="`brand_${brand.id}`" 
                    :value="`${brand.id}`" 
                    x-model="selectedBrands"
                >
                <label style="cursor: pointer;" class="form-check-label" :for="`brand_${brand.id}`" x-text="brand.name"></label>
            </div>
        </template>
    </div>
    <p> selected = <span x-text="selectedBrands" ></span> </p>
</div>
