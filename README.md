# Livewire coupons
### Project to test how live wire and alpine helps to create a coupon related website


# Steps to serve this project on ur local machine

```bash
git clone https://bitbucket.org/ThisIsntMySecondId/livewirecoupons/src/master/

cp .env.example .env

php artisan key:generate

# update db credentials

php artisan migrate

php artisan db:seed

php artisan serve
```